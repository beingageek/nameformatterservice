# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* This is the code for the Name Formatter Service, available at http://nameformatter-beingageek.rhcloud.com
* Version 0.1

### How do I get set up? ###

* What will you need?
    + JDK installed and configured
    + A Java code editor or IDE, preferably Eclipse
    + An application server, e.g., Apache Tomcat
* Configuration
    + <details coming soon>
* Dependencies
    + You need Jersey and JUnit, both are already specified in the pom file
* Database configuration
    + None
* How to run tests
    + Run the JUnit test class in src/test/java
* Deployment instructions
    + From Eclipse workspace, right-click on the NameFormatterService project and select *"Run As -> Maven Build"*
    + In the *"Goals"*, enter `clean package`

### Contribution guidelines ###

* Writing tests
    + <details coming soon>
* Code review
    + <details coming soon>
* Other guidelines
    + <details coming soon>

### Who do I talk to? ###

* For any questions email me at: rishikantaet@gmail.com