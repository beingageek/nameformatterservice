package com.bag.nfs.web;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;

import com.bag.nfs.util.FormattingUtility;

/**
 * @author BeingAGeek
 *
 */
@Path("/formatthisname")
public class NameFormatter {

	@GET
	@Produces("text/plain")
	public Response formatName() {
		String result = FormattingUtility.getFormattedName(null);
		return Response.status(200).entity(result).build();
	}

	@Path("{inputName}")
	@GET
	@Produces("text/plain")
	public Response formatInputName(
			@PathParam("inputName") String inputName
			) {

		String result = FormattingUtility.getFormattedName(inputName);
		return Response.status(200).entity(result).build();
	}
	
	@Path("/format")
	@GET
	@Produces("text/plain")
	public Response formatInputNameQuery(
			@QueryParam("name") String inputName
			) {

		String result = FormattingUtility.getFormattedName(inputName);
		return Response.status(200).entity(result).build();
	}

}
