package com.bag.nfs.util;

/**
 * @author BeingAGeek
 *
 */
public class FormattingUtility {
	
	/**
	 * Formats member name into proper case.
	 * @param name
	 * @return String - formatted name
	 */
	public static final String getFormattedName(String inputName) {
		if (!isValidString(inputName))
			return "Please provide a valid input";
		else {
			StringBuilder strBld = new StringBuilder();			
			try{
				// Convert to upper case first
				String name = inputName.toUpperCase();
				
				// + is acceptable as a space character
				name = name.replaceAll("\\+", " ");
				
				if (name.contains(chuckNorris) || name.contains(rajinikanth))
					return inputName;
				
				// Split by space
				String[] strArr = name.split(" ");
				for (int i=0; i<strArr.length; i++) {
					if ("".equals(strArr[i]))
						continue;
					// Handle -
					if (strArr[i].contains("-")) {
						if (0 != i)
							strBld.append(" ");
						String[] specPart = strArr[i].split("-");
						for (int j = 0; j < specPart.length; j++){
							if (0 != j)
								strBld.append("-");
							specPart[j] = specPart[j].trim();
							strBld.append(specPart[j].charAt(0));
							strBld.append(specPart[j].substring(1).toLowerCase());
						}
					} 
					// Handle names starting with Mc
					else if (strArr[i].matches("MC\\w+")) {
						if (0 != i)
							strBld.append(" ");
						strBld.append("Mc");
						int ind = strArr[i].indexOf("MC");
						strBld.append(strArr[i].substring(ind+2, ind+3).toUpperCase());
						strBld.append(strArr[i].substring(ind +3).toLowerCase());
					} 
					// Handle names starting with Mc
					else if (strArr[i].matches("DE\\w+")) {
						if (0 != i)
							strBld.append(" ");
						strBld.append("De");
						int ind = strArr[i].indexOf("DE");
						strBld.append(strArr[i].substring(ind+2, ind+3).toUpperCase());
						strBld.append(strArr[i].substring(ind +3).toLowerCase());
					}
					// Handle names starting with Mc
					else if (strArr[i].matches("DI\\w+")) {
						if (0 != i)
							strBld.append(" ");
						strBld.append("Di");
						int ind = strArr[i].indexOf("DI");
						strBld.append(strArr[i].substring(ind+2, ind+3).toUpperCase());
						strBld.append(strArr[i].substring(ind +3).toLowerCase());
					}
					// all other cases
					else {
						if (0 != i)
							strBld.append(" ");
						strArr[i] = strArr[i].trim();
						strBld.append(strArr[i].charAt(0));
						strBld.append(strArr[i].substring(1).toLowerCase());
					}
				}				
				return strBld.toString();
			} catch (Exception e) {
				e.printStackTrace();
				return inputName;
			}
		}
	}

	/**
	 * Checks for validity of a string.
	 * 
	 * <pre>
	 * isValidString() = false;
	 * isValidString("") = false;
	 * isValidString(" ") = false;
	 * isValidString("null") = true;
	 * isValidString("  null  ") = true;
	 * isValidString("bob") = true;
	 * isValidString("  bob   ") = true;
	 * </pre>
	 * @param str
	 * @return
	 */
	private static boolean isValidString(String str){
		if (null == str)
			return false;
		else if ("".equals(str.trim()))
			return false;
		else
			return true;
	}
	
	//private static String[] specialCases = {"MC", "DE", "DI"};
	
	private static String chuckNorris = "CHUCK NORRIS";
	
	private static String rajinikanth = "RAJINIKANTH";

}
